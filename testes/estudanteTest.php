<?php
    use PHPUnit\Framework\TestCase;

     require_once(dirname(__FILE__, 2).'/classes/Student.class.php');

    class studantTest extends TestCase{
        protected $estudante;

        protected function setUp(): void {
            $this ->estudante = new Student();
        }

        public function testSetName(){
            $this->estudante->setName("Victor");
            $this->assertEquals("Victor",  $this->estudante->getName());
        }
    }

?>