<?php
    use PHPUnit\Framework\TestCase;

    require_once(dirname(__FILE__, 3).'/classes/Student.class.php');

    class studentIntegracaoTest extends TestCase{
        protected $estudante;

        protected function setUp(): void {
            $this->estudante = new Student();
        }

        public function testIntegration()
        {
            $this->estudante->setName("Victor Hugo");
            $this->estudante->setEmail("victor.hugo@energisa.com.br");
            $retorno = $this->estudante->create();
            $this->assertTrue($retorno, "Dados não foram cadastrados corretamente");
        }


    }



?>